package com.lioncomsoft.airsensortest;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    UsbManager mUsbManager;
    UsbDevice device;
    UsbDeviceConnection usbConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        HashMap<String, UsbDevice> usbDevices = mUsbManager.getDeviceList();
        if (!usbDevices.isEmpty()) {

            // first, dump the hashmap for diagnostic purposes
            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                Log.d("debug", String.format("USBDevice.HashMap (vid:pid) (%X:%X)-%b class:%X:%X name:%s",
                        device.getVendorId(), device.getProductId(),
                        UsbSerialDevice.isSupported(device),
                        device.getDeviceClass(), device.getDeviceSubclass(),
                        device.getDeviceName()));
            }

            for (Map.Entry<String, UsbDevice> entry : usbDevices.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                int devicePID = device.getProductId();

//                if (deviceVID != 0x1d6b && (devicePID != 0x0001 && devicePID != 0x0002 && devicePID != 0x0003) && deviceVID != 0x5c6 && devicePID != 0x904c) {
                if (UsbSerialDevice.isSupported(device)) {
                    // There is a supported device connected - request permission to access it.
                    usbConnection = mUsbManager.openDevice(device);
                    break;
                } else {
                    usbConnection = null;
                    device = null;
                }
            }
//            if (device==null) {
//                // There are no USB devices connected (but usb host were listed). Send an intent to MainActivity.
//                Intent intent = new Intent(ACTION_NO_USB);
//                sendBroadcast(intent);
//            }
        }

        if(usbConnection!=null && device!=null) {
            UsbSerialDevice serial = UsbSerialDevice.createUsbSerialDevice(device, usbConnection);
            serial.open();
            serial.setBaudRate(9600);
            serial.setDataBits(UsbSerialInterface.DATA_BITS_8);
            serial.setStopBits(UsbSerialInterface.STOP_BITS_1);
            serial.setParity(UsbSerialInterface.PARITY_NONE);
            serial.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
            serial.read(mCallback);
        }
    }

    private UsbSerialInterface.UsbReadCallback mCallback = arg0 -> {
        Log.d("DEBUG", arg0.toString());
        // Code here :)
    };

//    UsbDevice findDevice() {
//        for (UsbDevice usbDevice: mUsbManager.getDeviceList().values()) {
//            if (usbDevice.getDeviceClass() == UsbConstants.) {
//                return usbDevice;
//            } else {
//                UsbInterface usbInterface = findInterface(usbDevice);
//                if (usbInterface != null) return usbDevice;
//            }
//        }
//        return null;
//    }
//
//    UsbInterface findInterface(UsbDevice usbDevice) {
//        for (int nIf = 0; nIf < usbDevice.getInterfaceCount(); nIf++) {
//            UsbInterface usbInterface = usbDevice.getInterface(nIf);
//            if (usbInterface.getInterfaceClass() == UsbConstants.USB_CLASS_PRINTER) {
//                return usbInterface;
//            }
//        }
//        return null;
//    }
}
